import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../shared/auth.service';
import { CartService } from '../shared/cart.service';
import { order, product, viewToken } from '../shared/model';
import { NotificationService } from '../shared/notification.service';
import { ProductService } from '../shared/product.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  products:any;
  infoForm: FormGroup;
  order!:order;
  showOrder=false;
  showInfo = false;
  userId:any;

  constructor(private fb: FormBuilder, private productService: ProductService, private cartService: CartService,
    private notification:NotificationService, private auth: AuthService) {
    this.infoForm = this.fb.group({
      name:['', Validators.required],
      phone:['', Validators.required],
      address:['', Validators.required],
    });
   }

  ngOnInit(): void {
    if(this.auth.getToken() !=  null){
      var decoded = JSON.parse(window.atob(this.auth.getToken().split('.')[1]));
      this.userId = decoded[viewToken.Id];
      this.products = this.cartService.items;
    }    
    else{
      this.products = this.cartService.items;
      }
  }

  get name(){
    return this.infoForm.get('name');
  }

  get phone(){
    return this.infoForm.get('phone');
  }

  get address(){
    return this.infoForm.get('address');
  }

  updateQuantity(id:any,quantity:any){ 
    var productExisting = this.products.find((x:product)=>x.id == id);

    if(productExisting){
      productExisting.quantity = Number(quantity);
    }
  }
  deleteProduct(item: any) {
    if(this.auth.getToken() != null){
      var decoded = JSON.parse(window.atob(this.auth.getToken().split('.')[1]));
        this.cartService.removeCartDatabase(decoded[viewToken.Id], item.id).subscribe(res=>{
            if(res.status == 200){
              const index = this.products.indexOf(item);
              if (index > -1) {
                this.products.splice(index, 1);
                this.cartService.remove(item);
              }
              }
            else
                this.notification.show('error','Delete is failed');
        })
    }else{
      const index = this.products.indexOf(item);
      if (index > -1) {
        this.products.splice(index, 1);
        this.cartService.remove(item);
      }
    } 
  }
  sumOfPrice():number{
    var sum= 0;
    for (const iterator of this.products) {
      sum += iterator.price* iterator.quantity* ((100-iterator.promotionPrice)/100);
    }

    return sum;
  }

  addOrder() {
    if(this.auth.getToken() != null){
      var decoded = JSON.parse(window.atob(this.auth.getToken().split('.')[1]));
      this.order = {
        nameUser:this.infoForm.get('name')?.value,
        phone: this.infoForm.get('phone')?.value,
        address: this.infoForm.get('address')?.value,
        createdOn:new Date(),
        updatedOn:new Date(),
        status:false,
        userId:decoded[viewToken.Id],
        products: this.products,
     }
    }else{
      this.order = {
        nameUser:this.infoForm.get('name')?.value,
        phone: this.infoForm.get('phone')?.value,
        address: this.infoForm.get('address')?.value,
        createdOn:new Date(),
        updatedOn:new Date(),
        status:false,
        userId:'',
        products: this.products,
     }
    }
   this.productService.addOrder(this.order).subscribe(res => {
     if (res.status == 200) {
       this.notification.show("success","Order is successfuly");
       this.showOrder = true;
       this.showInfo = false;
       this.cartService.removeAll();
     }
     else {
      this.notification.show("success","Order is failed");
     }
   })
 }

}
