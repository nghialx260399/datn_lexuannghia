import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { CartService } from '../shared/cart.service';
import { CategoryService } from '../shared/category.service';
import { viewToken, product } from '../shared/model';
import { NotificationService } from '../shared/notification.service';
import { ProductService } from '../shared/product.service';

@Component({
  selector: 'app-product-level',
  templateUrl: './product-level.component.html',
  styleUrls: ['./product-level.component.css']
})
export class ProductLevelComponent implements OnInit {

  productsBestSold:any;
  productsLast:any;
  productBestPromotion:any;
  categories:any;
  cp:number=1;
  pageSize = 12;
  pageSize1 = this.pageSize;
  countCart:any;
  notFound = false;
  categoryName:any;
  userId:any;
  
  constructor(private productService:ProductService, private categoryService: CategoryService, private noticeService:NotificationService,
    private cartService: CartService, private auth:AuthService) {
      if(this.auth.getToken() != null)
      {
        var decoded = JSON.parse(window.atob(this.auth.getToken().split('.')[1]));
        this.userId = decoded[viewToken.Id];
        this.cartService.getCart(this.userId).subscribe(res=>{
          if(res.status == 200){
            this.cartService.setItems(res.body);
            this.cartService.syncItems();
          }
        })
      }   
   }

  ngOnInit(): void {
    this.getProductsBestPromotion();
    this.getProductsBestSold();
    this.getProductsLast();
    this.getCategories();
  }

  getCategories(){
    this.categoryService.getCategories().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.categories=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }  

  getProductsBestSold(){
    this.productService.getProductsBestSold().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.productsBestSold=res.body;
       this.notFound = false;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  getProductsLast(){
    this.productService.getProductsLast().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.productsLast=res.body;
       this.notFound = false;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  getProductsBestPromotion(){
    this.productService.getProductsPromotionBest().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.productBestPromotion=res.body;
       this.notFound = false;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  // getProductsByCategoryId(id:number, name:string){
  //   this.productService.getProductsByCategoryId(id).subscribe(res=>{
  //     console.log(res);
  //    if(res.status==200){
  //      this.products=res.body;
  //      this.categoryName = name;
  //      this.products.length > 8 ? this.pageSize = 8 : this.pageSize = this.products.length;
  //      this.notFound = false;
  //      this.cp = 1;

  //      if(this.products.length == 0)
  //       this.notFound = true;
  //    }
  //    else{
  //      alert('Get data from serve  failed');
  //    }
  //   })
  // }

  // getProductsByName(name:string){
  //   if(name == ''){
  //     this.productService.getProducts().subscribe(res=>{
  //       console.log(res);
  //      if(res.status==200){
  //        this.products=res.body;
  //        this.notFound = false;
  //      }
  //      else{
  //        alert('Get data from serve  failed');
  //      }
  //     })
  //   }
  //   else{
  //     this.productService.getProductsByName(name).subscribe(res=>{
  //       console.log(res);
  //      if(res.status==200){
  //        this.products=res.body;
  //        this.cp = 1;
  //        if(this.products.length == 0)
  //           this.notFound = true;
  //      }
  //      else{
  //        alert('Get data from serve  failed');
  //      }
  //     })
  //   }
  // }
  addToCart(product:product){
    if(this.auth.getToken() != null){
      var decoded = JSON.parse(window.atob(this.auth.getToken().split('.')[1]));
        var obj1 = {
          userId:decoded[viewToken.Id],
          productId:product.id,
          quantity:1
        }
        this.cartService.addCartDatabase(obj1).subscribe(res=>{
            if(res.status == 200)
            {
                var obj = {
                  id:product.id,
                  name:product.name,
                  price:product.price,
                  promotionPrice:product.promotionPrice,
                  quantity:1,
                  size:product.size,
                  color:product.color,
                  image:product.image,
                  description:product.description,
                  categoryId:product.categoryId,
                }

                this.cartService.add(obj);
            }
            else
                this.noticeService.show('error','Add is failed');
        })
    }else{
    var obj={
      id:product.id,
      name:product.name,
      price:product.price,
      promotionPrice:product.promotionPrice,
      quantity:1,
      size:product.size,
      color:product.color,
      image:product.image,
      description:product.description,
      categoryId:product.categoryId,
      
    };

    this.cartService.add(obj);
    this.countCart = this.cartService.length;
    }
  }

}
