export interface product{
    id:number;
    name:string;
    quantity:number;
    price:number;
    image:string;
    description:string;
    categoryId:number;
    promotionPrice:number;
    createdOn:Date;
    updatedOn:Date;
    size:string;
    color:string;
}

export interface category{
    id:number;
    name:string;
    createdOn:Date;
    updatedOn:Date;
}

export interface order{
    nameUser:string;
    phone:string;
    address:string;
    products:product[];
    status:boolean;
    userId:string;
    createdOn:Date;
    updatedOn:Date;
  }

export class viewToken{
    static role='http://schemas.microsoft.com/ws/2008/06/identity/claims/role';
    static email= 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress';
    static username='http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name';
    static Id='http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier';
}

export interface cart{
    
}