import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { product } from './model';
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient:HttpClient) { }

  getProducts(): Observable<HttpResponse<product[]>> {
    const apiUrl = 'https://localhost:44305/api/Products/GetAllDistinct';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  getProductsBestSold(): Observable<HttpResponse<product[]>> {
    const apiUrl = 'https://localhost:44305/api/Products/GetBestSold';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  getProductsLast(): Observable<HttpResponse<product[]>> {
    const apiUrl = 'https://localhost:44305/api/Products/GetLast';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  getProductsPromotionBest(): Observable<HttpResponse<product[]>> {
    const apiUrl = 'https://localhost:44305/api/Products/GetBestPromotion';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  getProductBySize(size:string, productName:string, color:string): Observable<HttpResponse<product>> {
    const apiUrl = `https://localhost:44305/api/Products/GetBySize/${size}/${productName}/${color}`;

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product>(apiUrl, options);
  }

  getProductByColor(size:string, productName:string, color:string): Observable<HttpResponse<product>> {
    const apiUrl = `https://localhost:44305/api/Products/GetByColor/${color}/${productName}/${size}`;

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product>(apiUrl, options);
  }

  getProductsById(id:number): Observable<HttpResponse<product[]>> {

    const apiUrl = `https://localhost:44305/api/Products/GetById/${id}`;
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  getProductsByCategoryId(id:number): Observable<HttpResponse<product[]>> {
    const apiUrl = `https://localhost:44305/api/Products/SearchByCategory/${id}`;

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  getProductsByName(name:string): Observable<HttpResponse<product[]>> {
    const apiUrl = `https://localhost:44305/api/Products/SearchByName/${name}`;

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  addOrder(formData: any) {
    const data = JSON.stringify(formData);
    const apiUrl = 'https://localhost:44305/api/Orders/Create';
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.post(apiUrl, data, options);
  }
}
