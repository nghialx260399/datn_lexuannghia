import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiUrl="https://localhost:44305/api";
  constructor(private httpClient:HttpClient, private route:Router) { }

  login(formData:any){
    var option={
      headers:new HttpHeaders({
        'Content-Type':'application/json'
      }),
      responseType:'text' as const
    }
    return this.httpClient.post(this.apiUrl+'/Accounts/Login',formData,option);
  }

  register(formData:any){
    const data = JSON.stringify(formData);
    const apiUrl = 'https://localhost:44305/api/Accounts/Register';
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.post(apiUrl, data, options);
  }

  logout(){
    localStorage.removeItem('token');
  }

    getToken():any{
     return localStorage.getItem('token');
    }

    navigateToLogin(){
      this.route.navigateByUrl('/login');
    }
}
