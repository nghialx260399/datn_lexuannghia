import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { product, viewToken } from './model';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private _items:product[] = [];
  userId:any;

  constructor(private noticeService: NotificationService, private httpClient:HttpClient, private auth:AuthService) {
      this._items = JSON.parse(localStorage.getItem('items') ||'[]'); // get the data at lunch 
  }

  setItems(item:any){
    this._items = item;
  }

  getCart(userId:any):Observable<HttpResponse<product[]>> {
    const apiUrl = `https://localhost:44305/api/Carts/GetCart/${userId}`;
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  addCartDatabase(formData: any) {
    const data = JSON.stringify(formData);
    const apiUrl = 'https://localhost:44305/api/Carts/Create';
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.post(apiUrl, data, options);
  }

  removeCartDatabase(userId:any, productId:any) {
    const apiUrl = `https://localhost:44305/api/Carts/Delete/${userId}/${productId}`;
    return this.httpClient.delete(apiUrl, { observe: 'response' });
  }

  remove(item:any) {
    const index = this._items.indexOf(item);
    this._items.splice(index,1);
    this.syncItems();
  }

  removeAll(){
    localStorage.removeItem('items');
  }

  add(item:any) { 
    if(this._items.length == 0)
    {
      this._items.push(item as product);
      this.noticeService.show('success', 'Added ' + item.name + ' success');
    }
    else
    {
      var existing = this._items.find(x=>x.id == item.id);
      if(existing){
        existing.quantity += item.quantity;
        this.noticeService.show('success', 'Added ' + item.name + ' success');
      }else{
        this._items.push(item as product);
        this.noticeService.show('success', 'Added ' + item.name + ' success');
      }
    }
     this.syncItems();
  }

  get length() : number{
    return this._items.length
  }

  get items(){
    return this._items.slice(0)
  }

  syncItems(){
    localStorage.setItem('items',JSON.stringify(this._items));

  }
}
