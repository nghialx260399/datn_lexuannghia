import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import jwt_decode from "jwt-decode";
import { viewToken } from '../shared/model';
import { CartService } from '../shared/cart.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;

  constructor(private fb:FormBuilder,private authService:AuthService,private route:Router) {
    this.loginForm= this.fb.group({
      email:[''],
      password:['']
    })
  }

  submit(){
    this.authService.login(this.loginForm.value).subscribe(res=>{
      localStorage.setItem('token',res);
      this.route.navigateByUrl('/home');
    })
  }

  ngOnInit(): void {
  }

}
