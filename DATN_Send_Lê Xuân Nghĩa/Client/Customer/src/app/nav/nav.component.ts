import { ThrowStmt } from '@angular/compiler';
import { Component, DoCheck, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { CartService } from '../shared/cart.service';
import { viewToken } from '../shared/model';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit, DoCheck {

  countCart = 0;
  username:any;
  userId:any;
  products:any;

  constructor(private cartService: CartService, private auth: AuthService, private route:Router) {
   }
  ngDoCheck(): void {
    if(this.countCart != this.cartService.length)
      this.countCart = this.cartService.length;
  }

  ngOnInit(): void {
    if(this.auth.getToken() != null){
      var decoded = JSON.parse(window.atob(this.auth.getToken().split('.')[1]));
      this.username = decoded[viewToken.username];
      this.userId = decoded[viewToken.Id];
      this.cartService.syncItems();
      this.countCart  = this.cartService.length;
    }
      else{
        this.countCart  = this.cartService.length;
      }
  }

  logout(){
    //var decoded = jwt_decode(this.authService.getToken()?.toString());
    this.auth.logout();
    this.route.navigateByUrl('/home');
    this.username = null;
  }

}
