import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { CartService } from '../shared/cart.service';
import { product, viewToken } from '../shared/model';
import { NotificationService } from '../shared/notification.service';
import { ProductService } from '../shared/product.service';

@Component({
  selector: 'app-dialog-product-detail',
  templateUrl: './dialog-product-detail.component.html',
  styleUrls: ['./dialog-product-detail.component.css', './dialog-product-detail1.component.css']
})
export class DialogProductDetailComponent implements OnInit {

  id: any;
  product: any;
  numberOfQuantity=1;
  colorActive:any;
  sizeActive:any;

  constructor(private route: ActivatedRoute, private productService: ProductService, private cartService:CartService, private notifi:NotificationService,
    private auth:AuthService) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getProductById();
  }

  ngOnInit(): void {
  }

  getProductById() {
    this.productService.getProductsById(this.id).subscribe(res => {
      if (res.status == 200) {
        this.product = res.body;
        this.colorActive = this.product.color;
        this.sizeActive = this.product.size;
      }
      else {
        console.log('get product by id failed!');
      }

    })
  }

  getProductBySize(productName:string, size:string, color:string) {
    this.productService.getProductBySize(size, productName, color).subscribe(res => {
      if (res.status == 200) {
        var body = res.body as product
        if(body.id > 0){
            this.product = res.body;
            this.colorActive = this.product.color;
            this.sizeActive = this.product.size;
        }
        else
            this.notifi.show('error', 'Not found product');
      }
      else {
        this.notifi.show('error', 'Not found product');
      }

    })
  }

  getProductByColor(productName:string, size:string, color:string) {
    this.productService.getProductByColor(size, productName, color).subscribe(res => {
      if (res.status == 200) {
        var body = res.body as product
        if(body.id > 0)
        {
            this.product = res.body;
            this.colorActive = this.product.color;
            this.sizeActive = this.product.size;
        }
        else
            this.notifi.show('error', 'Not found product');
      }
      else {
        this.notifi.show('error', 'Not found product');
      }

    })
  }

  changeQuantityPlus(){
    this.numberOfQuantity++;
  }

  changeQuantityEx(){
    this.numberOfQuantity--;
  }

  addToCart(product:product){
    if(this.auth.getToken() != null){
      var decoded = JSON.parse(window.atob(this.auth.getToken().split('.')[1]));
        var obj1 = {
          userId:decoded[viewToken.Id],
          productId:product.id,
          quantity:1
        }
        this.cartService.addCartDatabase(obj1).subscribe(res=>{
            if(res.status == 200)
            {
                var obj = {
                  id:product.id,
                  name:product.name,
                  price:product.price,
                  promotionPrice:product.promotionPrice,
                  quantity:1,
                  size:product.size,
                  color:product.color,
                  image:product.image,
                  description:product.description,
                  categoryId:product.categoryId,
                }

                this.cartService.add(obj);
            }
            else
                this.notifi.show('error','Add is failed');
        })
    }else{
    var obj={
      id:product.id,
      name:product.name,
      price:product.price,
      promotionPrice:product.promotionPrice,
      quantity:1,
      size:product.size,
      color:product.color,
      image:product.image,
      description:product.description,
      categoryId:product.categoryId,
      
    };

    this.cartService.add(obj);
    }
  }

}
