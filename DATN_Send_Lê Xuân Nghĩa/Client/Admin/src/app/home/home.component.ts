import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from '../shared/category.service';
import { OrderService } from '../shared/order.service';
import { ProductService } from '../shared/product.service';
import { UserService } from '../shared/user.service';
import { ChartDataset, ChartOptions } from 'chart.js';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  orders:any;
  products:any;
  productBestSold:any;
  productWarning:any;
  categories:any;
  cp=1;
  users:any;
  statistical: any;

  constructor(private router:Router, private orderService:OrderService, private productService:ProductService,
              private categoryService:CategoryService, private userService:UserService) { }

  ngOnInit(): void {
    if(localStorage.getItem('token') == null)
      this.router.navigateByUrl('/login');
    this.getOrders();
    this.getCategories();
    this.getProducts();
    this.getProductsBestSold();
    this.getWarning();
    this.getUsers();
    this.getStatistical();
  }

  getOrders(){
    this.orderService.getOrders().subscribe(res=>{
     if(res.status==200){
       this.orders=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  getStatistical(){
    this.productService.getStatistical().subscribe(res=>{
      if(res.status==200){
        this.statistical=res.body;
      }
      else{
        alert('Get data from serve  failed');
      }
     })
  }
  
  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  getProducts(){
    this.productService.getProducts().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.products=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  getWarning(){
    this.productService.getWaring().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.productWarning=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  getProductsBestSold(){
    this.productService.getProductsBestSold().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.productBestSold=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  getCategories(){
    this.categoryService.getCategories().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.categories=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  getUsers(){
    this.userService.getUsers().subscribe(res=>{
     if(res.status==200){
       this.users=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

}
