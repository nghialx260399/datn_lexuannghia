import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { order } from '../shared/model';
import { NotificationService } from '../shared/notification.service';
import { OrderService } from '../shared/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  orders:any; 
  cp:number=1;
  constructor(private orderService:OrderService, private noticeService:NotificationService, private router:Router) { }

  ngOnInit(): void {
    if(localStorage.getItem('token') == null)
      this.router.navigateByUrl('/login');
    this.getOrders();
  }

  penddingNumber():number{
    return this.orders.filter((x:order)=>x.status == false).length;
  }

  approveNumber():number{
    return this.orders.filter((x:order)=>x.status == true).length;
  }

  rejectNumber():number{
    return this.orders.filter((x:order)=>x.status == null).length;
  }

  getOrders(){
    this.orderService.getOrders().subscribe(res=>{
     if(res.status==200){
       this.orders=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  updateOrder(id:any){
    this.orderService.updateOrder(id).subscribe(res=>{
    console.log(res);
      if(res.status==200){
        this.noticeService.show('success', 'Approve is successfuly');
        this.getOrders();
      }
      else{
        this.noticeService.show('error', 'Approve is failed');
      }
    })
}

rejectOrder(id:any){
  this.orderService.rejectOrder(id).subscribe(res=>{
    console.log(res);
      if(res.status==200){
        this.noticeService.show('success', 'Reject is successfuly');
        this.getOrders();
      }
      else{
        this.noticeService.show('error', 'Reject is failed');
      }
    })
}

getOrderByPhone(phone:any){
  
}
}
