import { CommonModule } from '@angular/common';
import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AddCategoryComponent } from './add-category/add-category.component';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { CategoryComponent } from './category/category.component';
import { CreateProductComponent } from './create-product/create-product.component';
import { DetailOrderComponent } from './detail-order/detail-order.component';
import { EditComponent } from './edit/edit.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { OrderComponent } from './order/order.component';
import { ProductComponent } from './product/product.component';
import { RegisterComponent } from './register/register.component';
import { UpdateCategoryComponent } from './update-category/update-category.component';

const routes: Routes = [

  {path: '', redirectTo: 'admin/home', pathMatch: 'full'},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'admin',component:AdminLayoutComponent},
  {path: '',
         loadChildren: () => import('./admin-layout/admin-layout.module').then(m => m.AdminLayoutModule)
       },
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
