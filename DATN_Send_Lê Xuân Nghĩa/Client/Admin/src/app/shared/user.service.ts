import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { role, user } from './model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient:HttpClient) { }

  getUsers(): Observable<HttpResponse<user[]>> {
    const apiUrl = 'https://localhost:44305/api/Accounts/GetAllUsers';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<user[]>(apiUrl, options);
  }

  getRolesByUserId(id:string): Observable<HttpResponse<role[]>> {
    const apiUrl = `https://localhost:44305/api/Accounts/GetRoles/${id}`;

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<role[]>(apiUrl, options);
  }

  updateRoles(formData:any, userId:string){
    var data= JSON.stringify(formData);
    const apiUrl= `https://localhost:44305/api/Accounts/UpdateRoles/${userId}`;
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.put(apiUrl,data, options);

  }
}
