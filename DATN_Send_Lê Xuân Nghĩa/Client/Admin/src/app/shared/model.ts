export interface product{
    id:number;
    name:string;
    quantity:number;
    price:number;
    image:string;
    description:string;
    categoryId:number;
    promotionPrice:number;
    createdOn:Date;
    updatedOn:Date;
    size:string;
    color:string;
}

export interface category{
    id:number;
    name:string;
    createdOn:Date;
    updatedOn:Date;
}

export interface order{
    id:number;
    nameUser:string;
    phone:string;
    address:string;
    createdOn:Date;
    updatedOn:Date;
    status:boolean;
    userId:string;
    products:product[];
}

export interface orderBasic{
    id:number;
    nameUser:string;
    phone:string;
    address:string;
    createdOn:Date;
    updatedOn:Date;
    status:boolean;
}

export interface user{
    userId:string;
    firstName:string;
    lastName:string;
    userName:string;
    email:string;
    roles:string[];
}

export interface role{
    roleId:string;
    roleName:string;
    selected:boolean;
}

export interface statistical{
    month:number;
    sales:number;
}