import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { order, orderBasic } from './model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private httpClient:HttpClient) { }

  getOrders(): Observable<HttpResponse<orderBasic[]>> {
    const apiUrl = 'https://localhost:44305/api/Orders/GetAll';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<orderBasic[]>(apiUrl, options);
  }

  getOrderById(id:number): Observable<HttpResponse<order[]>> {
    const apiUrl = `https://localhost:44305/api/Orders/GetById/${id}`;
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<order[]>(apiUrl, options);
  }

  updateOrder(id:any){
    var data= JSON.stringify(id);
    console.log(data);
    
    const apiUrl = `https://localhost:44305/api/Orders/Update/${id}`;
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.put(apiUrl,data, options);

  }

  rejectOrder(id:any){
    var data= JSON.stringify(id);
    
    const apiUrl = `https://localhost:44305/api/Orders/Reject/${id}`;
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.put(apiUrl,data, options);

  }
}
