import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from '../shared/notification.service';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  users:any; 
  cp:number=1;
  notFound=false;
  constructor(private router:Router, private userService:UserService, private noticeService:NotificationService) { }

  ngOnInit(): void {
    if(localStorage.getItem('token') == null)
      this.router.navigateByUrl('/login');
    this.getUsers();
  }

  getUsers(){
    this.userService.getUsers().subscribe(res=>{
     if(res.status==200){
       this.users=res.body;
       this.notFound = false;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }
}
