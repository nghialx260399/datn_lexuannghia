import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { CategoryService } from '../shared/category.service';
import { NotificationService } from '../shared/notification.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {
  category: any;
  categoryForm: FormGroup;
  constructor(private route: ActivatedRoute, private categoryService: CategoryService, private fb: FormBuilder,private router:Router,
    private noticeService:NotificationService) {
    this.categoryForm = this.fb.group({
      name:['', Validators.required],
    });
   }

  ngOnInit(): void {
  }

  Create(){
    if(!this.categoryForm.invalid){
      this.categoryService.addCategory(this.categoryForm.value).subscribe(res=>{
        if(res.status==200){
          this.noticeService.show('success', 'Create successfully');
          this.router.navigateByUrl('/admin/category');
        }
        else{
          this.noticeService.show('error', 'Create fail');
        }
       })
    }
    else{
      alert('Validate form error');
    }

  }

  get name(){
    return this.categoryForm.get('name');
  }
}
