import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { NotificationService } from '../shared/notification.service';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  id: any;
  roles: any;
  constructor(private userService:UserService, private sanitizer: DomSanitizer, private route: ActivatedRoute, private notifi:NotificationService,
    private router:Router) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getRoles();
   }

  ngOnInit(): void {
    if(localStorage.getItem('token') == null)
    this.router.navigateByUrl('/login');
  }

  checkRole(item:any){
    item.selected = !item.selected;
  }

  updateRoles(){

    this.userService.updateRoles(this.roles, this.id).subscribe(res=>{
      if(res.status == 200){
        this.notifi.show('success', 'Update roles is successfully');
        this.router.navigateByUrl('/admin/user');
      }
      else{
        this.notifi.show('error', 'Update roles is failed');
      }
    })
    
  }
  getRoles(){
    this.userService.getRolesByUserId(this.id).subscribe(res=>{
      if(res.status == 200){
        this.roles = res.body;
      }
    })
  }
}
